import React, { Component, useState, useEffect } from "react";
import ReactDOM from "react-dom";
require ("../scss/main.scss")


function sortJSON(data, key) {
  return JSON.parse(JSON.stringify(data)).sort((a, b) => {
    const x = a[key]; const y = b[key];
    return ((x < y) ? -1 : ((x > y) ? 1 : 0))
  });
}

function filterJSON(data, key1, key2, keyVal) {
  if (keyVal == 0) {
    return data
  }
  return data.filter(n => {
    return (n[key1].includes(keyVal) || n[key2].includes(keyVal))
  })
}


const App = () => {
  const [users, setUsers] = useState([])
  const contacts = [];
  const [input, setInput] = useState("");


  useEffect(() => {
    fetch('https://teacode-recruitment-challenge.s3.eu-central-1.amazonaws.com/users.json')
      .then(res => res.json())
      .then(res => setUsers(sortJSON(res, 'last_name')))
      .catch(error => console.warn(error))
  }, [])


  function handleChange(e, id) {
    if (e.target.checked) {
      contacts.push(id)
    } else {
      contacts.splice(contacts.indexOf(id), 1);
    }
    console.log(contacts);
  }

  function handleInput(e) {
    setInput(e.target.value)
  }

  return (
    <>
      <input type="text" onChange={handleInput} value={input} />
      <ul>
        {filterJSON(users, 'first_name', 'last_name', input).map(el => {
          return <li key={el.id} >
            <img src={el.avatar} />
            <span>{el.first_name} {el.last_name}</span>
            <input type="checkbox" onChange={e => { handleChange(e, el.id) }} />
          </li>
        })}
      </ul>
    </>
  )
}

ReactDOM.render(<><App /></>, document.getElementById("app"));
